/**
*      @author Shashank Shekhar
*      @date   02/05/2014
*      @description    Trigger function class for the Account_Hierarchy trigger.

       Modification Log:
       ------------------------------------------------------------------------------------
       Developer                       Date                Description
       ------------------------------------------------------------------------------------
       Shashank Shekhar               02/05/2014          Original Version 
	   Manohar Reddy				  11/13/2015		  Edited method createAHLRecord for DEF-009704

*/ 
public class Account_Hierarchy_TriggerFunctions {
    public static Boolean BoolGlobalParentChanged=false;
    public static list<Account_Hierarchy__c> lstAHId=new list<Account_Hierarchy__c>();  
    public static set<Id> set_GlobalParetntId = new set<Id>();
    public static Boolean Boolean_InvalidGlobalParent=false;
    
    /*
    *  Method name:  createAHLRecord
    *  @description  Method to create AHL record on creation of Account hierarchy record.
    *  @param      List<Account_Hierarchy__c> List of Account Hierarchy which are inserted.
    */
    public static void createAHLRecord(List<Account_Hierarchy__c> lst_newAH)
    {
        
        //Declaring variables
        List<Account_Hierarchy_Link__c> lst_AHL=new List<Account_Hierarchy_Link__c>();
        List<Account_Hierarchy__c> lst_AH=new List<Account_Hierarchy__c>();
        Account_Hierarchy_Link__c obj_AHL;
        map<Id, Id> map_AccIdAHId = new map<Id, Id>();
        RecordType rType = New RecordType();
        
        rType = [Select Id,DeveloperName from RecordType where DeveloperName = :StaticConstants.RTYPE_ACCOUNTHIERACHY_PRIMARYHIERARCHY_DNAME]; // added for DEF-009704
       
        // Loop over the Account Hierarchy record and separate them based on record type
        for(Account_Hierarchy__c obj_AH : lst_newAH){
                    
            if(obj_AH.RecordtypeId== rType.Id)
            {
               
                map_AccIdAHId.put(obj_AH.Global_Parent__c, obj_AH.Id);
                
            }
            lst_AH.add(obj_AH);
        }
        
        checkGlobalParentAccount(map_AccIdAHId, lst_newAH);
        
        // Create child record of Account Hierarchy Link when an account Hierarchy record is created and passes above check. 
        if(!Boolean_InvalidGlobalParent){
            for(Account_Hierarchy__c obj_AH : lst_AH){
                obj_AHL=new Account_Hierarchy_Link__c();
                obj_AHL.Account__c=obj_AH.Global_Parent__c;
                obj_AHL.Account_Hierarchy__c=obj_AH.Id;
                lst_AHL.add(obj_AHL);
            }
        }else
        return;
                
        if(!lst_AHL.isEmpty())
            insert lst_AHL;
        
        
       
    }
    
    /*
    *  Method name:  updateAHLRecord
    *  @description  Method to update AHL record on updation of Account hierarchy record.
    *  @param      Map<Id,Account_Hierarchy__c> New map of Account Hierarchy which are updated.
    *  @param      Map<Id,Account_Hierarchy__c> Old map of Account Hierarchy which are updated.
    */
    public static void updateAHLRecord(map<id,Account_Hierarchy__c> newMapObjAH, map<Id,Account_Hierarchy__c> oldMapObjAH){
    
        //
        list<Account_Hierarchy_Link__c> lstObjAHL=new List<Account_Hierarchy_Link__c>();
        Account_Hierarchy_Link__c objAHL=new Account_Hierarchy_Link__c();
        map<Id, Account_Hierarchy__c> map_AH = new map<Id, Account_Hierarchy__c>();
        List<Account_Hierarchy__c> lst_newAH=new List<Account_Hierarchy__c>();
        map<Id, Id> map_AccIdAHId = new map<Id, Id>();
        
        //
        for(Account_Hierarchy__c obj_AH: newMapObjAH.values()){
            if(newMapObjAH.get(obj_AH.Id).Global_Parent__c != oldMapObjAH.get(obj_AH.Id).Global_Parent__c){
                map_AH.put(obj_AH.Id, obj_AH);
                map_AccIdAHId.put(obj_AH.Global_Parent__c, obj_AH.Id);
            }
            system.debug('records in lst_newAH is ' + lst_newAH.size());
            lst_newAH.add(obj_AH);
        }   
        
        
        checkGlobalParentAccount(map_AccIdAHId, lst_newAH);
        
        // call method to validate the Account hierarchy for which AHL records can be updated
        
        if(!Boolean_InvalidGlobalParent){
            lstAHId=validateAccountHierarchyChange(newMapObjAH,map_AH);
            
            //
            if(!lstAHId.isEmpty()){
                for(Account_Hierarchy_Link__c AHL: [select Id,Account_Hierarchy__c,Account__c,Immediate_Parent__c, Name, Global_Parent__c from Account_Hierarchy_Link__c 
                                                    where Account_Hierarchy__c IN: lstAHId order by Name] ){
                                                        
                                                        //update Account of AHL records on change of the Global parent of Hierarchy                
                                                        if(AHL.Immediate_Parent__c==null ){
                                                            AHL.Account__c=newMapObjAH.get(AHL.Account_Hierarchy__c).Global_Parent__c;
                                                        }
                                                        //update Immediate parent of AHL records on change of the Global parent of Hierarchy 
                                                        if(AHL.Immediate_Parent__c!=null && AHL.Immediate_Parent__c==oldMapObjAH.get(AHL.Account_Hierarchy__c).Global_Parent__c){
                                                            AHL.Immediate_Parent__c=newMapObjAH.get(AHL.Account_Hierarchy__c).Global_Parent__c;
                                                        }
                                                        // add all the AHL records to be updated in a list.
                                                        lstObjAHL.add(AHL);
                                                    }
            }
            if(!lstObjAHL.isEmpty()){
                update lstObjAHL;           
            }
        }           
    }
    
    /*
    *  Method name:  validateAccountHierarchyChange
    *  @description  Method to check that Global Parent is not referenced in Account Hierarchy Link record. This will avoid the loop structure when Global parent in an Account Hierarchy record is updated.
    *  @param        Map<Id,Account_Hierarchy__c> New map of Account Hierarchy which are updated.
    *  @param        Map<Id,Account_Hierarchy__c> Old map of Account Hierarchy which are updated.
    *  @return       List<Account_Hierarchy__c> List of Account Hierarchy records for which AHL are to be updated.
    */    
    public static list<Account_Hierarchy__c> validateAccountHierarchyChange (map<Id,Account_Hierarchy__c> mapNewAH, map<Id, Account_Hierarchy__c> map_AH){
    
        //
        
        map<Id,Set<Id>> map_AhAHL =new map<Id,Set<Id>>(); 
        
        // iterate on AHL and store Account Hierarchy and list of corresponding AHL's Accounts in a map
        for(Account_Hierarchy_Link__c AHL:[select id, Account__c,Immediate_Parent__c,Account_Hierarchy__c,Account_Hierarchy__r.id from Account_Hierarchy_Link__c 
                                           where Account_Hierarchy__c=: map_AH.keyset()]){
                                               
                                               if(map_AhAHL.containsKey(AHL.Account_Hierarchy__c))
                                                   map_AhAHL.get(AHL.Account_Hierarchy__c).add(AHL.Account__c);
                                               else
                                                   map_AhAHL.put(AHL.Account_Hierarchy__c,new set<Id>{AHL.Account__c});
                                           }
        
        // Iterate over New map and Old map and check where the Global parents are changed
        system.debug('mapNewAH is'+mapNewAH.keyset());
        for(String s:mapNewAH.keyset()){
            system.debug('map_AhAHL.get(s) is'+map_AhAHL.get(s)+'mapNewAH.get(s).Global_Parent__c is'+mapNewAH.get(s).Global_Parent__c+'s is'+s);
            if(!map_AhAHL.isEmpty() && map_AhAHL.get(s)!=null && map_AhAHL.get(s).contains(mapNewAH.get(s).Global_Parent__c) && s != mapNewAH.get(s).Global_Parent__c){
            //Add an error when new Global Parent is present in one of the AHLs 
                mapNewAH.get(s).addError(Label.Cannot_change_global_parent);
                
            }
            else if(!map_AhAHL.isEmpty()&&  map_AhAHL.get(s)!=null && !map_AhAHL.get(s).contains(mapNewAH.get(s).Global_Parent__c)){
                lstAHId.add(mapNewAH.get(s));
                set_GlobalParetntId.add(mapNewAH.get(s).Global_Parent__c);
                BoolGlobalParentChanged=true;
            }
        }
        
        return lstAHId;
    }
    
    
    /*
    *  Method name:  checkGlobalParentAccount
    *  @description  Method to check that Global Parent of Account hierarchy is not being used in anyother Primary Hierarchy.
    *  @param        map<Id, Id> New map of Account Id and Account Hierarchy ID.
    *  @param        set<Id> Set of Account Id.
    *  @return       List<Account_Hierarchy__c> List of Account Hierarchy records equivalent to Trigger.New.
    */
    public static void checkGlobalParentAccount (map<Id, Id> map_AccIdAHId, List<Account_Hierarchy__c> lst_newAH){
        
    
        // Throw error and return for Primary hierarchy whose Global Parent is referred in another primary Hierarchy
        for(AggregateResult ar : [Select Count(Id) iCount, a.Account__c From Account_Hierarchy_Link__c a where 
                                    a.Hierarchy_Type__c =: StaticConstants.RTYPE_ACCOUNTHIERACHY_PRIMARYHIERARCHY AND a.Account__c IN: map_AccIdAHId.keyset() GROUP BY a.Account__c]){
            if((Integer)ar.get('iCount')>0){
                for(Account_Hierarchy__c obj_AH : lst_newAH)
                    if(obj_AH.Id == map_AccIdAHId.get((string)ar.get('Account__c')))    
                        obj_AH.addError(Label.Global_parent_cannot_be_reused_in_primary_hierarchy); 
                        Boolean_InvalidGlobalParent= true;      
                        return;
            }
            else{
                Boolean_InvalidGlobalParent= false;
            }
        }    
    } 
}